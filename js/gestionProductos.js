var products;

function getProducts() {
  var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Products";

  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
       // Typical action to be performed when the document is ready:
       console.log(request.responseText);
       products = request.responseText;
       processProducts();
    }
  };
  request.open("GET", url, true);
  request.send();
}

function processProducts() {
  var JSONProductos = JSON.parse(products);
  var table = document.getElementById("table");
  if(table.getElementsByTagName("thead")[0]){
    table.removeChild(table.getElementsByTagName("thead")[0]);
    table.removeChild(table.getElementsByTagName("tbody")[0]);
  }
  var thead = document.createElement("thead");
  var head = document.createElement("td");
  head.innerText = "Nombre";
  thead.appendChild(head);
  head = document.createElement("td");
  head.innerText = "Precio";
  thead.appendChild(head);
  head = document.createElement("td");
  head.innerText = "Stock";
  thead.appendChild(head);
  table.appendChild(thead);
  var tbody = document.createElement("tbody");
  table.appendChild(tbody);
  for (var i = 0; i < JSONProductos.value.length; i++) {
    var row = document.createElement("tr");
    var col = document.createElement("td");
    col.innerText = JSONProductos.value[i].ProductName;
    row.appendChild(col);
    col = document.createElement("td");
    col.innerText = JSONProductos.value[i].UnitPrice;
    row.appendChild(col);
    col = document.createElement("td");
    col.innerText = JSONProductos.value[i].UnitsInStock;
    row.appendChild(col);

    tbody.appendChild(row);
  }
}
