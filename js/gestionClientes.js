var clients;
var flagUrl = "https://www.countries-ofthe-world.com/flags-normal/flag-of-{}.png"
function getClients() {
  var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Customers";

  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
       // Typical action to be performed when the document is ready:
       console.log(request.responseText);
       clients = request.responseText;
       processClients();
    }
  };
  request.open("GET", url, true);
  request.send();
}

function processClients(){
  var JSONProductos = JSON.parse(clients);
  var table = document.getElementById("table");
  if(table.getElementsByTagName("thead")[0]){
    table.removeChild(table.getElementsByTagName("thead")[0]);
    table.removeChild(table.getElementsByTagName("tbody")[0]);
  }
  var thead = document.createElement("thead");
  var head = document.createElement("td");
  head.innerText = "Nombre";
  thead.appendChild(head);
  head = document.createElement("td");
  head.innerText = "Dirección";
  thead.appendChild(head);
  head = document.createElement("td");
  head.innerText = "Ciudad";
  thead.appendChild(head);
  head = document.createElement("td");
  head.innerText = "País";
  thead.appendChild(head);
  table.appendChild(thead);
  var tbody = document.createElement("tbody");
  table.appendChild(tbody);
  for (var i = 0; i < JSONProductos.value.length; i++) {
    var row = document.createElement("tr");
    var col = document.createElement("td");
    col.innerText = JSONProductos.value[i].ContactName;
    row.appendChild(col);
    col = document.createElement("td");
    col.innerText = JSONProductos.value[i].Address;
    row.appendChild(col);
    col = document.createElement("td");
    col.innerText = JSONProductos.value[i].City;
    row.appendChild(col);

    col = document.createElement("td");
    flag = document.createElement("img");
    var country = JSONProductos.value[i].Country;
    if(country === "UK"){
      country = "United-Kingdom"
    }
    flag.src = flagUrl.replace("{}",country);
    flag.classList.add("flag");
    col.appendChild(flag);
    row.appendChild(col);

    tbody.appendChild(row);
  }
}
